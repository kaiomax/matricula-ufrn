package br.ufrn.imd.matricula.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@NamedQueries({
	@NamedQuery(name = "findAllSelected", query = "SELECT s FROM Subject as s WHERE s.selected is true"),
	@NamedQuery(name = "findAllValid", query = "SELECT s FROM Subject as s INNER JOIN s.component c WHERE c.fulfilled is null")
})
public class Subject {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_SUBJECT")
	@SequenceGenerator(name="SEQ_SUBJECT", sequenceName="seq_subject", allocationSize=1)
	private Integer id;
	
	@OneToOne
	private Component component;

	private String time;
	private String teacher;
	private Integer priority;
	private String comment;
	private Boolean selected;
	
	public Subject() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
}
