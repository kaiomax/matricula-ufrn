package br.ufrn.imd.matricula.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Component {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_COMPONENT")
	@SequenceGenerator(name="SEQ_COMPONENT", sequenceName="seq_component", allocationSize=1)
	private Integer id;

	private String name;
	private String code;
	private Integer hours;
	private Boolean fulfilled;
	private Boolean required;
	private String preRequirements;
	private String coRequirements;
	private String equivalences;

	public Component() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public Boolean getFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(Boolean fulfilled) {
		this.fulfilled = fulfilled;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public String getPreRequirements() {
		return preRequirements;
	}

	public void setPreRequirements(String preRequirements) {
		this.preRequirements = preRequirements;
	}
	
	public String getCoRequirements() {
		return coRequirements;
	}

	public void setCoRequirements(String coRequirements) {
		this.coRequirements = coRequirements;
	}

	public String getEquivalences() {
		return equivalences;
	}

	public void setEquivalences(String equivalences) {
		this.equivalences = equivalences;
	}
}