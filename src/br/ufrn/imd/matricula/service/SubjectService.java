package br.ufrn.imd.matricula.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufrn.imd.matricula.data.SubjectDAO;
import br.ufrn.imd.matricula.model.Component;
import br.ufrn.imd.matricula.model.Subject;

@Stateless
public class SubjectService {
	@Inject
	private SubjectDAO DAO;
	
	@Inject
	private ComponentService componentService;
	
	@PersistenceContext
	private EntityManager em;
	
	public Subject findById(Integer id) {
		return DAO.findById(id);
	}
	
	public List<Subject> getSubjectList() {
		List<Subject> subjectList = DAO.findAll();
		
		subjectList.sort((s1, s2) -> s1.getComponent().getName().compareTo(s2.getComponent().getName()));
		
		return subjectList;
	}
	
	public List<Subject> getSubjectSuggestionList() {	
		List<Component> componentList = componentService.getComponentsFulfilledWithEquivalences();
		List<String> collect = componentList.stream()
				.map(Component::getEquivalences)
				.collect(Collectors.toList());
		String equivalences = collect.stream()
		        .reduce("", (str, item) -> {
		            return str.concat(item);
		        });
		
		List<Subject> subjectList = DAO.getValidSubjects();

		subjectList = subjectList.stream()
                .filter((s) -> !equivalences.contains(s.getComponent().getCode()))
                .collect(Collectors.toList());
		
		subjectList.sort((s1, s2) -> s1.getComponent().getName().compareTo(s2.getComponent().getName()));

		return subjectList;
	}
	
	public List<Subject> getSubjectSelectedList() {	
		List<Subject> subjectList = DAO.getSelectedSubjects();

		subjectList.sort((s1, s2) -> s1.getComponent().getName().compareTo(s2.getComponent().getName()));

		return subjectList;
	}
	
	public void save(Subject s) {
		if(s.getId() == null) {
			DAO.add(s);
		} else {
			DAO.update(s);
		}
	}
	
	public void delete(Subject s) {
		DAO.delete(s);
	}
	
	public void toggleSelected(Integer id) {
		Subject subject = findById(id);
		if(subject.getSelected() == null) {
			subject.setSelected(true);
		} else {
			subject.setSelected(!subject.getSelected());
		}	
		DAO.update(subject);
	}
}
