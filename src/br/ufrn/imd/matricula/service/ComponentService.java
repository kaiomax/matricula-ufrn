package br.ufrn.imd.matricula.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.ufrn.imd.matricula.data.ComponentDAO;
import br.ufrn.imd.matricula.model.Component;

@Stateless
public class ComponentService {
	@Inject
	private ComponentDAO DAO;
	
	public Component findById(Integer id) {
		return DAO.findById(id);
	}
	
	public List<Component> getComponentList() {
		List<Component> componentList = DAO.findAll();
		
		componentList.sort((c1, c2) -> c1.getName().compareTo(c2.getName()));
		
		return componentList;
	}
	
	
	public List<Component> getComponentsFulfilledWithEquivalences() {
		return DAO.getComponentsFulfilledWithEquivalences();
	}

	public void save(Component c) {
		if(c.getId() == null) {
			DAO.add(c);
		} else {
			DAO.update(c);
		}
	}
	
	public void delete(Component c) {
		DAO.delete(c);
	}
}
