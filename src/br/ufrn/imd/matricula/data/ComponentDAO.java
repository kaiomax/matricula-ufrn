package br.ufrn.imd.matricula.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.ufrn.imd.matricula.model.Component;

@Stateless
public class ComponentDAO extends DAO<Component>{
	public ComponentDAO() {
		super(Component.class);
	}
	
	public List<Component> getComponentsFulfilledWithEquivalences() {
		TypedQuery<Component> filterComponentsQuery = em.createQuery("SELECT c FROM Component as c WHERE c.fulfilled is not null AND c.equivalences is not null", Component.class);
		return filterComponentsQuery.getResultList();
	}
}
