package br.ufrn.imd.matricula.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

public abstract class DAO<T> {

	private final Class<T> class_;
	
	@PersistenceContext
	protected EntityManager em;

	public DAO(Class<T> class_) {
		this.class_ = class_;
	}

	public void add(T t) {
		em.persist(t);
	}

	public void delete(T t) {
		em.remove(em.merge(t));
	}

	public void update(T t) {
		em.merge(t);
	}

	public List<T> findAll() {
		CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(class_);
		query.select(query.from(class_));

		List<T> list = em.createQuery(query).getResultList();

		return list;
	}

	public T findById(Integer id) {
		T instance = em.find(class_, id);
		return instance;
	}
}
