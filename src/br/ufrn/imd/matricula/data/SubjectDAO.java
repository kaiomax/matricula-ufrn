package br.ufrn.imd.matricula.data;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.ufrn.imd.matricula.model.Subject;

@Stateless
public class SubjectDAO extends DAO<Subject> {
	public SubjectDAO() {
		super(Subject.class);
	}
	
	public List<Subject> getValidSubjects() {
		TypedQuery<Subject> query = em.createNamedQuery("findAllValid", Subject.class);
		return query.getResultList();
	}
	
	public List<Subject> getSelectedSubjects() {
		TypedQuery<Subject> query = em.createNamedQuery("findAllSelected", Subject.class);
		return query.getResultList();
	}
}
