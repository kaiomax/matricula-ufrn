package br.ufrn.imd.matricula.bean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.ufrn.imd.matricula.model.Component;
import br.ufrn.imd.matricula.service.ComponentService;

@RequestScoped
@Named
public class ComponentBean {
	@Inject
	private ComponentService service;
	private Component component = new Component();
	private List<Component> componentList = new ArrayList<>();

	public Component getComponent() {
		return component;
	}

	public List<Component> getComponentList() {
		componentList = service.getComponentList();		
		return componentList;
	}

	public void setComponentList(List<Component> componentList) {
		this.componentList = componentList;
	}
	
	public String save() {
		service.save(this.component);
		return "componentes";
	}
	
	public String edit(Integer id) {
		this.component = service.findById(id);
		return "componente";
	}
	
	public String delete(Component c) {
		service.delete(c);
		return "componentes";
	}
}
