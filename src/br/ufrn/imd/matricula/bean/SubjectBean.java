package br.ufrn.imd.matricula.bean;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufrn.imd.matricula.model.Component;
import br.ufrn.imd.matricula.model.Subject;
import br.ufrn.imd.matricula.service.ComponentService;
import br.ufrn.imd.matricula.service.SubjectService;

@RequestScoped
@Named
public class SubjectBean {
	@Inject
	private ComponentService componentservice;
	@Inject
	private SubjectService subjectService;

	private Subject subject = new Subject();
	private List<Subject> subjectList = new ArrayList<>();
	private Integer componentId;

	@PersistenceContext
	private EntityManager em;
	
	public Subject getSubject() {
		return subject;
	}
	
	public List<Subject> getSubjectList() {
		subjectList = subjectService.getSubjectList();
		return subjectList;
	}

	public void setSubjectList(List<Subject> subjectList) {
		this.subjectList = subjectList;
	}

	public List<Subject> getSubjectSuggestionList() {	
		return subjectService.getSubjectSuggestionList();
	}
	
	public List<Subject> getSubjectSelectedList() {	
		return subjectService.getSubjectSelectedList();
	}
	
	public List<Component> getComponentList() {
		return componentservice.getComponentList();
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public void saveComponent() {
		Component component = componentservice.findById(this.componentId);
		this.subject.setComponent(component);
	}
	
	public String save() {
		Component component = componentservice.findById(this.componentId);
		this.subject.setComponent(component);
		subjectService.save(this.subject);
		return "turmas";
	}
	
	public String edit(Integer id) {
		this.subject = subjectService.findById(id);
		this.componentId = this.subject.getComponent().getId();
		return "turma";
	}
	
	public String delete(Subject s) {
		subjectService.delete(s);
		return "turmas";
	}
	
	public void select(Integer id) {
		subjectService.toggleSelected(id);
	}
}
